# Git First project
This is the example repository for the [Git Quick Start](https://www.juniordevelopercentral.com/git-quick-start/) tutorial on the [Junior Developer Central](https://www.juniordevelopercentral.com/) website.

The idea is that you clone this project and add your own comments.  If you raise a pull request, i'll then add this to the comments section.